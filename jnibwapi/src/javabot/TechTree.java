package javabot;

import java.util.ArrayList;
import java.util.HashMap;

import javabot.TechTree.TechNode;
import javabot.model.Unit;
import javabot.types.TechType.TechTypes;
import javabot.types.UnitType;
import javabot.types.UnitType.UnitTypes;
import javabot.types.UpgradeType.UpgradeTypes;

public class TechTree {
	
	private static TechTree singleton = null;
	
	public class TechNode
	{
		ArrayList<TechNode> requirements;
		int builtAt;
		boolean building;
		boolean tech;
		boolean unit;
		int code;
		UnitType unitType;
		public TechNode upperNode() {
			return find(builtAt, true, false, false);
		}
	}

	ArrayList<TechNode> techs;
	JNIBWAPI bwapi;

	public static TechTree getInstance(JNIBWAPI bwapi){
		if(singleton == null)
			singleton = new TechTree(bwapi);
		return singleton;
	}
	
	private TechTree(JNIBWAPI bwapi)
	{
		techs = new ArrayList<TechNode>();
		this.bwapi = bwapi;
		addUpgrades();
		addUnits();
		addBuildings();
		addAbilities();
		//TODO: rest of all the things
	}
	private void addAbilities() {
		// TODO Auto-generated method stub

	}
	private void addBuildings() {
		//Nexus
		TechNode t = new TechNode();
		t.building = true;
		t.requirements = null;
		t.code =  UnitTypes.Protoss_Nexus.ordinal();
		t.unitType = bwapi.getUnitType(t.code);
		techs.add(t);
		//Gateway
		t = new TechNode();
		t.building = true;
		t.requirements = new ArrayList<TechNode>();
		t.requirements.add(find(UnitTypes.Protoss_Nexus.ordinal(), true, false, false));
		t.code =  UnitTypes.Protoss_Gateway.ordinal();
		t.unitType = bwapi.getUnitType(t.code);
		techs.add(t);
		//Forge
		t = new TechNode();
		t.building = true;
		t.requirements = new ArrayList<TechNode>();
		t.requirements.add(find(UnitTypes.Protoss_Nexus.ordinal(), true, false, false));
		t.code =  UnitTypes.Protoss_Forge.ordinal();
		t.unitType = bwapi.getUnitType(t.code);
		techs.add(t);
		//CyberCore
		t = new TechNode();
		t.building = true;
		t.requirements = new ArrayList<TechNode>();
		t.requirements.add(find(UnitTypes.Protoss_Gateway.ordinal(), true, false, false));
		t.code =  UnitTypes.Protoss_Cybernetics_Core.ordinal();
		t.unitType = bwapi.getUnitType(t.code);
		techs.add(t);
		//Citadel of Adun
		t = new TechNode();
		t.building = true;
		t.requirements = new ArrayList<TechNode>();
		t.requirements.add(find(UnitTypes.Protoss_Cybernetics_Core.ordinal(), true, false, false));
		t.code =  UnitTypes.Protoss_Citadel_of_Adun.ordinal();
		t.unitType = bwapi.getUnitType(t.code);
		techs.add(t);
		//Templar Archives
		t = new TechNode();
		t.building = true;
		t.requirements = new ArrayList<TechNode>();
		t.requirements.add(find(UnitTypes.Protoss_Citadel_of_Adun.ordinal(), true, false, false));
		t.code =  UnitTypes.Protoss_Templar_Archives.ordinal();
		t.unitType = bwapi.getUnitType(t.code);
		techs.add(t);
	}
	private void addUnits() {
		//Probe
		TechNode t;
		t = new TechNode();
		t.unit = true;
		t.requirements = null;
		t.builtAt =  UnitTypes.Protoss_Nexus.ordinal();
		t.code =  UnitTypes.Protoss_Probe.ordinal();
		t.unitType = bwapi.getUnitType(t.code);
		techs.add(t);

		// Zealot
		t = new TechNode();
		t.unit = true;
		t.requirements = null;
		t.builtAt =  UnitTypes.Protoss_Gateway.ordinal();
		t.code =  UnitTypes.Protoss_Zealot.ordinal();
		t.unitType = bwapi.getUnitType(t.code);
		techs.add(t);

	}
	private void addUpgrades() {

	}
	public TechNode requires(int tech, boolean b, boolean te, boolean u)
	{
		TechNode t = find(tech, b, te, u);
		if(u || te)
		{
			boolean faux = true;
			for(Unit un : bwapi.getMyUnits())
				if(un.getTypeID() == t.builtAt)
					faux = false;
			if(faux)
				return find(t.builtAt, true, false, false);
		}
		if(t.requirements == null)
			return null;
		else
		{
			for(TechNode q : t.requirements)
			{
				boolean faux = true;
				for(Unit un : bwapi.getMyUnits())
					if(un.getTypeID() == q.code)
						faux = false;
				if(faux)
					if(requires(q.code, q.building, q.tech, q.unit) == null)
						return q;
					else
						return requires(q.code, q.building, q.tech, q.unit);
			}
			return null;
		}
	}
	public TechNode find(int code, boolean b, boolean te, boolean u) {
		for(TechNode t : techs)
			if( t.code == code &&  t.building == b && t.tech == te && t.unit == u )
				return t;
		System.out.println("Error in Tech Tree");
		System.exit(-1);
		return null;
	}
}
