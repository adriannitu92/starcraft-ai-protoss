package javabot;

import java.util.ArrayList;

import javabot.model.ChokePoint;
import javabot.model.Unit;
import javabot.types.UnitType.UnitTypes;
import javabot.util.BWColor;

public class ArmyManager {

	//TODO build cannons @ minerals 1- 
	//TODO build cannons @ ramps 2+

	//////////////////////////////////////////
	///TODO
	///rough estimate of a build schedule
	///get 1 DT for each harass squad
	///get observers up to 10 but dont parallelyze the build process, we want the number to scale in and only in late game
	///get equal number of dragoon and zealots if gas holds
	///else zealots for days
	//////////////////////////////////////////
	//TODO harass squad indepedent use act()
	//TODO at least 1 harass squad per detected expansion

	BaseManager b;
	JNIBWAPI bwapi;
	private TechTree t;
	ArrayList<Unit> units;
	ArrayList<Squad> squads;
	ArrayList<Unit> buildPlan; //TODO plan a build
	DebugManager debugManager;
	ChokePoint ccp;//closest choke

	public ArmyManager(JNIBWAPI bwapi, BaseManager baseManager, DebugManager debugManager) {
		this.bwapi = bwapi;
		this.b = baseManager;
		b.armyCallback(this);
		this.t = TechTree.getInstance(bwapi);
		units = new ArrayList<Unit>();
		squads = new ArrayList<Squad>();
		this.debugManager = debugManager;
	}

	public void act() {
		if(bwapi.getFrameCount() == 0){
			double dist = Double.MAX_VALUE;

			for(ChokePoint cp : bwapi.getMap().getChokePoints()){
				double valX = (cp.getCenterX() - b.location.getX());
				double valY = (cp.getCenterY() - b.location.getY());
				double val = (float) Math.sqrt(valX*valX + valY * valY);
				if(val < dist){
					dist = val;
					ccp = cp;
				}
			}
		}

		buildOrder();
		
		for(Unit u : units) //TODO move to debug
			bwapi.drawBox(u.getX()-10, u.getY()-10,u.getX() + 10, u.getY()+10, BWColor.RED, false, false);
		
		if(bwapi.getFrameCount() %30 == 0){
			for(Unit u : units)
				bwapi.move(u.getID(), ccp.getCenterX(), ccp.getCenterY());
		}

	}
	public void accept(int unitID) {
		units.add(bwapi.getUnit(unitID));
	}
	
	private void buildOrder(){
		if(units.size() < 20)
		{
			if(t.requires(UnitTypes.Protoss_Zealot.ordinal(), false, false, true) == null)
				b.request(t.find(UnitTypes.Protoss_Zealot.ordinal(), false, false, true));
			else
				b.request(t.requires(UnitTypes.Protoss_Zealot.ordinal(), false, false, true));
		}
	}
		
}
