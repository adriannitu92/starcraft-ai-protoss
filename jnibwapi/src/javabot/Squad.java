package javabot;

import java.awt.Point;
import java.util.ArrayList;
import javabot.model.BaseLocation;
import javabot.model.Unit;

public class Squad {

	//TODO hard feature : micro first step enable: frame counter to see time to next attack.
	//TODO hard hard feature: micro-stepbacks
	//TODO hard hard feature: micro-alignment
	
	
	private ArrayList<Unit> observers = null;
	private ArrayList<Unit> melee = null;
	private ArrayList<Unit> ranged = null;
	private boolean isHarasser = false;
	private boolean DTHarasser = false; //TODO detect
	private Point attackArea;
	private Point defendArea;
	private ArmyManager leader;
	private Squad harassers;
	
	
	public boolean hasObserver(){
		return (observers!= null) && (observers.size() > 0);
	}
	public boolean hasMelee(){
		return (melee!= null) && (melee.size() > 0);
	}
	public boolean hasRanged(){
		return (ranged!= null) && (ranged.size() > 0);
	}
	public boolean isHarass(){
		return isHarasser;
	}
	public void act(){
		//TODO
	}
	public Squad(ArmyManager armyManager, ArrayList<Unit> initialUnits)
	{
		leader = armyManager;
		observers = getObservers(initialUnits);
		melee = getMelee(initialUnits);
		ranged = getRanged(initialUnits);
	}
	private ArrayList<Unit> getRanged(ArrayList<Unit> initialUnits) {
		// TODO Auto-generated method stub
		return null;
	}
	private ArrayList<Unit> getMelee(ArrayList<Unit> initialUnits) {
		// TODO Auto-generated method stub
		return null;
	}
	private ArrayList<Unit> getObservers(ArrayList<Unit> initialUnits) {
		// TODO Auto-generated method stub
		return null;
	}
	public void move(Point p){
		//TODO death ball UNLESS retreat, use baits
	}
	public void attackArea(Point p){
		//TODO
	}
	public void attackArea(BaseLocation b){
		attackArea(new Point(b.getX(),b.getY()));
	}
	public boolean defends(){
		return defendArea != null;
	}
	public void defendArea(Point p){
		//TODO
	}
	public void defendArea(BaseLocation b){
		defendArea(new Point(b.getX(),b.getY()));
	}
	public void harass(BaseLocation b){
		//TODO
	}
	public void addUnits(ArrayList<Unit> units){
		//TODO
	}
	public ArrayList<Unit> disband(){
		ArrayList<Unit> ret = new ArrayList<Unit>();
		ret.addAll(observers);
		ret.addAll(melee);
		ret.addAll(ranged);
		observers = null;
		melee = null;
		ranged = null;
		return ret;
	}
	private void requestObserver(){
		//TODO
		//TODO callback
	}
	private void underAttack(){
		//TODO
		//TODO callback
	}
	private void lostAllUnits(){
		//TODO dont count observers
		//TODO callback
	}
}
