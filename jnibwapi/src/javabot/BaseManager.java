package javabot;

import java.awt.Point;
import java.util.ArrayDeque;
import java.util.ArrayList;
import javabot.TechTree.TechNode;
import javabot.model.BaseLocation;
import javabot.model.Unit;
import javabot.types.UnitType.UnitTypes;
import javabot.util.BWColor;

public class BaseManager {

	//TODO prevent overbuild
	//TODO detect variations of cancelation units // purpuose???
	//TODO rework the income estimation ? // how???
	//TODO scale the building of pylons with production facilities or we cant keep up
	//TODO pylon trick please, we need it HARD
	//TODO stop adding the gas early, stop sending probes THAT early to wait untill we finshed building an assimiliator

	ArrayList<Unit> units;
	ArrayList<Unit> gasProbes;
	public BaseLocation location; //TODO its ugly, fix this
	JNIBWAPI bwapi;
	ArrayDeque<Point> moneyFrames;
	ArrayList<BaseManager> expansions;
	ArrayList<Point> buildingBuildingsLocations;
	ArmyManager armyManager;
	boolean builtGas = false;
	int frame = 0;
	TechTree t;
	int secondsToEstimate = 10;
	DebugManager debugManager;

	public BaseManager(JNIBWAPI bwapi, ArrayList<Unit> arrayList, BaseLocation location, DebugManager debugManager) {
		this.bwapi = bwapi;
		this.location = location;
		units = new ArrayList<Unit>(arrayList);
		gasProbes = new ArrayList<Unit>();
		buildingBuildingsLocations = new ArrayList<Point>();
		this.t = TechTree.getInstance(bwapi);
		moneyFrames = new ArrayDeque<Point>();
		this.debugManager = debugManager;
	}

	public void act() {
		Point moneyPer30Frames = estimate();
		for (Unit unit : units) {
			if (unit.getTypeID() == UnitTypes.Protoss_Nexus.ordinal()) {
				if( unit.isBeingConstructed())
					return;
			}
		}
		//moneyPer30Frames.getX();//junk code, removes warnings TODO

		int nexusCount = 0;
		if(! this.isSaturated())
		{
			//move to debug TODO
			bwapi.drawCircle(location.getX(), location.getY(), 300, BWColor.YELLOW, false, false);
			for (Unit unit : units) {
				if (unit.getTypeID() == UnitTypes.Protoss_Nexus.ordinal()) {
					// if it's training queue is empty
					if (unit.getTrainingQueueSize() <= 1) {
						// check if we have enough minerals and supply, and (if we do) train one worker (Terran_SCV)
						if ((bwapi.getSelf().getMinerals() >= 50) && (bwapi.getSelf().getSupplyTotal()-bwapi.getSelf().getSupplyUsed() >= 2)) 
							bwapi.train(unit.getID(), UnitTypes.Protoss_Probe.ordinal());
					}
					nexusCount++;
				}
			}
			if(nexusCount == 0)
			{
				int cc = getUnitInRange(UnitTypes.Protoss_Nexus.ordinal(), 0, location);
				if(cc != -1)
					units.add(bwapi.getUnit(cc));
			}
		}
		else
		{
			//TODO move to debug manager
			bwapi.drawCircle(location.getX(), location.getY(), 300, BWColor.GREEN, false, false);
			boolean expand = true;
			if(expansions != null)
				for(BaseManager b : expansions)
					if(! b.isSaturated())
						expand = false;
			if (expand )
				attemptExpand();
		}
		for (Unit unit : units) {
			if (unit.getTypeID() == UnitTypes.Protoss_Probe.ordinal()) {
				//if it is idle (not doing anything),
				if (unit.isIdle()) {
					// then find the closest mineral patch (if we see any)
					int closestId = -1;
					double closestDist = 99999999;
					for (Unit neu : bwapi.getNeutralUnits()) {
						if (neu.getTypeID() == UnitTypes.Resource_Mineral_Field.ordinal()) {
							double distance = Math.sqrt(Math.pow(neu.getX() - unit.getX(), 2) + Math.pow(neu.getY() - unit.getY(), 2));
							if ((closestId == -1) || (distance < closestDist)) {
								closestDist = distance;
								closestId = neu.getID();
							}
						}
					}
					// and (if we found it) send this worker to gather it.
					if (closestId != -1) bwapi.rightClick(unit.getID(), closestId);
				}
			}
		}
		if (((bwapi.getSelf().getSupplyTotal() - bwapi.getSelf().getSupplyUsed())/2) < 5) {
			// Check if we have enough minerals,
			if (bwapi.getSelf().getMinerals() >= 100) {
				// try to find the worker near our home position
				int worker = getNearestUnit(UnitTypes.Protoss_Probe.ordinal(), location.getX(), location.getY());
				if (worker != -1) {
					// if we found him, try to select appropriate build tile position for supply depot (near our home base)
					Point buildTile = getBuildTile(worker, UnitTypes.Protoss_Pylon.ordinal(), location.getX(), location.getY());
					// if we found a good build position, and we aren't already constructing a Supply Depot, 
					// order our worker to build it
					if ((buildTile.x != -1) && (!weAreBuilding(UnitTypes.Protoss_Pylon.ordinal()))
							&&
							!alreadyBuilding(buildTile)) {
						if(bwapi.getUnit(worker).isGatheringMinerals())
						{
							bwapi.build(worker, buildTile.x, buildTile.y, UnitTypes.Protoss_Pylon.ordinal());
							buildingBuildingsLocations.add(buildTile);
						}
					}
				}
			}
		}
		if(bwapi.getSelf().getSupplyUsed() >= 30 && builtGas == false)
			if (bwapi.getSelf().getMinerals() >= 100) {
				// try to find the worker near our home position
				int worker = getNearestUnit(UnitTypes.Protoss_Probe.ordinal(), location.getX(), location.getY());
				if (worker != -1) {
					// if we found him, try to select appropriate build tile position for supply depot (near our home base)

					Point buildTile = getBuildTile(worker, UnitTypes.Protoss_Assimilator.ordinal(), location.getX(), location.getY());
					// if we found a good build position, and we aren't already constructing a Supply Depot, 
					// order our worker to build it
					if ((buildTile.x != -1) && (!weAreBuilding(UnitTypes.Protoss_Assimilator.ordinal()))
							&&
							!alreadyBuilding(buildTile)) {
						bwapi.build(worker, buildTile.x, buildTile.y, UnitTypes.Protoss_Assimilator.ordinal());
						buildingBuildingsLocations.add(buildTile);
						builtGas = true;
						while(gasProbes.size() < 3)
							for(Unit u : units)
								if( u.getTypeID() == UnitTypes.Protoss_Probe.ordinal())
								{
									gasProbes.add(u);
									units.remove(u);
									break;
								}

					}
				}
			}
		if(gasProbes.size() != 0)
			for(Unit u: bwapi.getMyUnits())
				if(u.getTypeID() == UnitTypes.Protoss_Assimilator.ordinal())
				{
					for(Unit v : gasProbes)
						if(v.isGatheringGas() == false)
							bwapi.rightClick(v.getID(), u.getID());
					break;
				}
		frame++;
		//		if(frame%90 == 0)
		//		{
		//			frame = 0;
		//			buildingBuildingsLocations = new ArrayList<Point>();
		//		}

		if(expansions!= null)
			for(BaseManager b : expansions)
				if( this != b)
					b.actE();
	}


	private Point estimate() {
		Point ret;
		Point q = new Point(bwapi.getSelf().getCumulativeMinerals(),bwapi.getSelf().getCumulativeGas());
		if(frame < 30 * secondsToEstimate) //estimate over the last 10 seconds
		{
			moneyFrames.add(q);
			return null;
		}
		else
		{
			Point p = moneyFrames.pop();
			moneyFrames.add(q);
			ret = new Point(q.x - p.x, q.y-p.y);
		}

		//		if(frame != 0 && frame %30 == 0)
		//			bwapi.printText(ret.x+" "+ret.y);
		return ret;
	}

	private void actE() {
		int nexusCount = 0;
		if(! this.isSaturated())
		{
			bwapi.drawCircle(location.getX(), location.getY(), 300, BWColor.YELLOW, false, false);
			for (Unit unit : units) {
				if (unit.getTypeID() == UnitTypes.Protoss_Nexus.ordinal()) {
					// if it's training queue is empty
					if (unit.getTrainingQueueSize() <= 1) {
						// check if we have enough minerals and supply, and (if we do) train one worker (Terran_SCV)
						if ((bwapi.getSelf().getMinerals() >= 50) && (bwapi.getSelf().getSupplyTotal()-bwapi.getSelf().getSupplyUsed() >= 2)) 
							bwapi.train(unit.getID(), UnitTypes.Protoss_Probe.ordinal());
					}
					nexusCount++;
				}
			}
			if(nexusCount == 0)
			{
				int cc = getUnitInRange(UnitTypes.Protoss_Nexus.ordinal(), 0, location);
				if(cc != -1)
					units.add(bwapi.getUnit(cc));
			}
		}
		else
			bwapi.drawCircle(location.getX(), location.getY(), 300, BWColor.GREEN, false, false);
		for (Unit unit : units) {
			if (unit.getTypeID() == UnitTypes.Protoss_Probe.ordinal()) {
				//if it is idle (not doing anything),
				if (unit.isIdle()) {
					// then find the closest mineral patch (if we see any)
					int closestId = -1;
					double closestDist = 99999999;
					for (Unit neu : bwapi.getNeutralUnits()) {
						if (neu.getTypeID() == UnitTypes.Resource_Mineral_Field.ordinal()) {
							double distance = Math.sqrt(Math.pow(neu.getX() - unit.getX(), 2) + Math.pow(neu.getY() - unit.getY(), 2));
							if ((closestId == -1) || (distance < closestDist)) {
								closestDist = distance;
								closestId = neu.getID();
							}
						}
					}
					// and (if we found it) send this worker to gather it.
					if (closestId != -1) bwapi.rightClick(unit.getID(), closestId);
				}
			}
		}
	}

	private void attemptExpand() {
		if(expansions == null)
			expansions = new ArrayList<BaseManager>();
		//find new expansion location

		BaseLocation expLocation = null;
		for(BaseLocation bl : bwapi.getMap().getBaseLocations())
			if(! bl.isIsland() )
				if(getUnitInRange(UnitTypes.Protoss_Nexus.ordinal(), 200, bl) == -1)
				{
					expLocation = bl;
					break;
				}
		//scout that location TODO

		//build it
		if(expLocation != null)
			if (bwapi.getSelf().getMinerals() >= 400 &&
			! alreadyBuilding( new Point (expLocation.getTx(), expLocation.getTy() ))) {
				// try to find the worker near our home position
				int worker = getNearestUnit(UnitTypes.Protoss_Probe.ordinal(), location.getX(), location.getY());
				if (worker != -1 && ! weAreBuilding(UnitTypes.Protoss_Nexus.ordinal())) {
					// if we found him, try to select appropriate build tile position for supply depot (near our home base) 
					// order our worker to build it
					bwapi.build(worker, expLocation.getTx(), expLocation.getTy(), UnitTypes.Protoss_Nexus.ordinal());
					buildingBuildingsLocations.add(new Point(expLocation.getTx(), expLocation.getTy()));
				}



				ArrayList<Unit> expUnits = new ArrayList<Unit>();
				expUnits.add(bwapi.getUnit(worker));

				BaseManager expansion = new BaseManager(bwapi, expUnits, expLocation, debugManager);
				expansions.add(expansion);
			}
	}

	private boolean alreadyBuilding(Point point) {
		for(Point p : buildingBuildingsLocations)
			if(p.x == point.x && p.y== point.y)
				return true;
		return false;
	}

	private int getNearestUnit(int unitTypeID, int x, int y) {
		int nearestID = -1;
		double nearestDist = 9999999;
		for (Unit unit : units) {
			if ((unit.getTypeID() != unitTypeID) || (!unit.isCompleted())) continue;
			double dist = Math.sqrt(Math.pow(unit.getX() - x, 2) + Math.pow(unit.getY() - y, 2));
			if (nearestID == -1 || dist < nearestDist) {
				nearestID = unit.getID();
				nearestDist = dist;
			}
		}
		return nearestID;
	}	

	// Returns the Point object representing the suitable build tile position
	// for a given building type near specified pixel position (or Point(-1,-1) if not found)
	// (builderID should be our worker)
	private Point getBuildTile(int builderID, int buildingTypeID, int x, int y) {
		Point ret = new Point(-1, -1);
		int maxDist = 3;
		int stopDist = 40;
		int tileX = x/32; int tileY = y/32;

		// Refinery, Assimilator, Extractor
		if (bwapi.getUnitType(buildingTypeID).isRefinery()) {
			for (Unit n : bwapi.getNeutralUnits()) {
				if ((n.getTypeID() == UnitTypes.Resource_Vespene_Geyser.ordinal()) && 
						( Math.abs(n.getTileX()-tileX) < stopDist ) &&
						( Math.abs(n.getTileY()-tileY) < stopDist )
						) return new Point(n.getTileX(),n.getTileY());
			}
		}

		while ((maxDist < stopDist) && (ret.x == -1)) {
			for (int i=tileX-maxDist; i<=tileX+maxDist; i++) {
				for (int j=tileY-maxDist; j<=tileY+maxDist; j++) {
					if (bwapi.canBuildHere(builderID, i, j, buildingTypeID, false)) {
						// units that are blocking the tile
						boolean unitsInWay = false;
						for (Unit u : bwapi.getAllUnits()) {
							if (u.getID() == builderID) continue;
							if ((Math.abs(u.getTileX()-i) < 4) && (Math.abs(u.getTileY()-j) < 4)) unitsInWay = true;
						}
						if (!unitsInWay) {
							ret.x = i; ret.y = j;
							return ret;
						}
						// psi power for Protoss (this seems to work out of the box)
						if (bwapi.getUnitType(buildingTypeID).isRequiresPsi()) {}
					}
				}
			}
			maxDist += 2;
		}

		if (ret.x == -1) bwapi.printText("Unable to find suitable build position for "+bwapi.getUnitType(buildingTypeID).getName());
		return ret;
	}

	// Returns true if we are currently constructing the building of a given type.
	private boolean weAreBuilding(int buildingTypeID) {
		for (Unit unit : units) {
			if ((unit.getTypeID() == buildingTypeID) && (!unit.isCompleted())) return true;
			if (bwapi.getUnitType(unit.getTypeID()).isWorker() && unit.getConstructingTypeID() == buildingTypeID) return true;
		}
		return false;
	}


	private int countResources( int unitType )
	{
		int count = 0;
		for(Unit u : bwapi.getNeutralUnits())
			if(u.getTypeID() == unitType )
				if(distance(location, u) <= 300)
				{
					count ++;
					bwapi.drawCircle(u.getX(), u.getY(), 30, BWColor.YELLOW, false, false);
				}
		return count;
	}

	private int getUnitInRange( int unitType, int range, BaseLocation bl)
	{
		for(Unit u : bwapi.getMyUnits())
			if(u.getTypeID() == unitType )
				if(distance(bl, u) <= range)
					return u.getID();
		for(Unit u : bwapi.getEnemyUnits())
			if(u.getTypeID() == unitType )
				if(distance(bl, u) <= range)
					return u.getID();
		return -1;
	}

	private int distance(BaseLocation location2, Unit u) {
		double x = location2.getX()- u.getX();
		double y = location2.getY()- u.getY();
		return (int) Math.floor(Math.sqrt(x*x + y*y));
	}

	public boolean isSaturated()
	{
		int minerals = countResources(UnitTypes.Resource_Mineral_Field.ordinal());
		int gases = countResources(UnitTypes.Resource_Vespene_Geyser.ordinal());
		int assimilators = getNearestUnit(UnitTypes.Protoss_Assimilator.ordinal(), location.getX(), location.getY());
		if( assimilators != -1)
			gases ++;
		int probeCount = 0;
		for(Unit u : units)
			if(u.getTypeID() == UnitTypes.Protoss_Probe.ordinal())
				probeCount++;
		if(probeCount >= 2.5 *minerals + 3*gases)
			return true;
		return false;
	}

	public void accept(int unitID) {
		if(bwapi.getUnit(unitID).getTypeID() ==  UnitTypes.Protoss_Probe.ordinal()) {
			if(expansions == null)
				units.add(bwapi.getUnit(unitID));
			else
			{
				float minDist = distance(location,bwapi.getUnit(unitID) );
				BaseManager loc = this;
				for(BaseManager b : expansions)
					if(minDist > distance(b.location, bwapi.getUnit(unitID)))
					{
						minDist = distance(b.location, bwapi.getUnit(unitID));
						loc = b;
					}
				loc.units.add(bwapi.getUnit(unitID));
			}
		}
		if(UnitTypes.Protoss_Zealot.ordinal() == bwapi.getUnit(unitID).getTypeID()) 
			armyManager.accept(unitID);
	}

	//Called by everybody else who wants something
	public void request(TechNode techNode) {
		if(techNode.building == true)
		{
			if (bwapi.getSelf().getMinerals() >= techNode.unitType.getMineralPrice()) {
				int worker = getNearestUnit(UnitTypes.Protoss_Probe.ordinal(), location.getX(), location.getY());
				if (worker != -1) {
					Point buildTile = getBuildTile(worker, techNode.code, location.getX(), location.getY());
					if ((buildTile.x != -1) && (!weAreBuilding(techNode.code))
							&&
							!alreadyBuilding(buildTile)) {
						if(bwapi.getUnit(worker).isGatheringMinerals())
						{
							bwapi.build(worker, buildTile.x, buildTile.y, techNode.code);
							buildingBuildingsLocations.add(buildTile);
						}
					}
				}
			}
		}
		else
			if(techNode.unit == true)
			{
				boolean faux = false;
				for(Unit u : bwapi.getMyUnits())//TODO optimize this loop
					if(u.getTypeID() == techNode.builtAt && u.getTrainingQueueSize() == 0)
					{
						bwapi.train(u.getID(), techNode.code);
						faux = true;
						break;
					}
				if(!faux)
					this.request(techNode.upperNode());
				//IF WE SHOULD!!!

			}

	}

	public void armyCallback(ArmyManager armyManager2) {
		armyManager = armyManager2;

	}

}
