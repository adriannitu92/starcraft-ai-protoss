package javabot;

import javabot.model.ChokePoint;
import javabot.model.Unit;
import javabot.util.BWColor;

public class DebugManager {

	JNIBWAPI bwapi;
	boolean chokePoints;
	boolean paths;
	boolean squadArea;
	boolean pylonRange;
	boolean detectRange;
	boolean workerSchedule;
	
	//TODO keep track of MULTIPLE BASES shit man 
	
	public DebugManager(JNIBWAPI bwapi, boolean printChokePoints, boolean printPaths, 
			boolean printSquadArea, boolean printPylonRange, boolean printDetectRange,
			boolean printWorkerSchedule){
		this.bwapi = bwapi;
		chokePoints = printChokePoints;
		paths = printPaths;
		squadArea = printSquadArea;
		pylonRange = printPylonRange;
		detectRange = printDetectRange;
		workerSchedule = printWorkerSchedule;
	}
	
	public void act() {
		drawStaticDefenceRange();
		if(workerSchedule)
			drawWorkers();
		if(chokePoints)
			drawChokePoints();
		if(paths)
			drawPaths();
		if(squadArea)
			drawSquadArea();
		if ( pylonRange)
			drawPylonRange();
		if(detectRange)
			drawDetectRange();		
	}
	private void drawDetectRange() {
		// TODO Auto-generated method stub
		
	}

	private void drawPylonRange() {
		// TODO Auto-generated method stub
		
	}

	private void drawSquadArea() {
		// TODO Auto-generated method stub
		
	}

	private void drawPaths() {
		// TODO Auto-generated method stub
		
	}

	private void drawChokePoints() {
		for(ChokePoint cp : bwapi.getMap().getChokePoints())
			bwapi.drawCircle(cp.getCenterX(), cp.getCenterY(), 150, BWColor.ORANGE, false, false);
		
	}

	void drawWorkers(){
		// Draw circles over workers (blue if they're gathering minerals, green if gas, yellow if they're constructing).
		for (Unit u : bwapi.getMyUnits())  { //optimize for less cycles and more ram make a array of PROBES TODO
			if (u.isGatheringMinerals()) bwapi.drawCircle(u.getX(), u.getY(), 12, BWColor.BLUE, false, false);
			else if (u.isGatheringGas()) bwapi.drawCircle(u.getX(), u.getY(), 12, BWColor.GREEN, false, false);
		}
	}
	void drawStaticDefenceRange(){
		//TODO cannon, missile turret, spine, spore
	};

	public void keyPressed(int keyCode) {
		switch(keyCode)
		{
		case '1': {chokePoints = ! chokePoints; break; }
		case '2': {paths = !paths; break;}
		case '3': {squadArea = !squadArea; break;}
		case '4': {pylonRange = ! pylonRange; break;}
		case '5': {detectRange = ! detectRange; break;}
		case '6': {workerSchedule = ! workerSchedule; break;}
		default: {break;}
		}
	}

}
